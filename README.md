# Before Golive Checklist
1. ~~Backup PHP files~~
1. ~~Prepare DAWAS~~
	1. ~~Prepare Integration folder~~
		1. ~~Create local user account through Computer Management -> System Tools -> Local Users and Groups~~
		2. ~~Share folder to user account created in step #1~~
	1. ~~Prepare DAWAS SQL User~~
		1. ~~Create Login in Security -> Logins, make sure `Enforce password policy`, `Enforce password expiration`, User must change password at next login is NOT checked~~
		2. ~~Create user linked to login in ePayment_live -> Security -> Users~~
1. ~~Install xampp 7.2.21~~
1. ~~Install SSL certificates~~
1. ~~Copy PHP files and apply migration~~
1. ~~Edit files~~:
	1. ~~`index.php`, above `ini_set('date.timezone',...)`, add: `require_once('classes/JWT.php');`~~
	1. ~~`config/config.php`, under 'ISC', add: `'Integration' => array('dir' => joinPaths('D:', 'EPAYMENTINTEGRATION')),`~~
	1. ~~FileWatcher/config.js, under `module.exports.directories`, add: `INTEGRATIONS: '\\\\10.59.240.174\\INTEGRATION',`~~
	1. ~~FileWatcher/config.js, under `module.exports.scripts`, add: `INTEGRATIONS_BATCH_FILE: 'D:\\xampp\\htdocs\\ePayment_tiga\\ScriptIntegration.bat',`~~
1. ~~Run Nessus scan~~
1. ~~Change FileWatcherDB to PHP~~
	1. ~~Install PHP on DB Server~~
	1. ~~Install Microsoft Visual C++ 2015 Redistributable Update 3~~
	1. ~~Change PI Config in ScriptToReadResponseDB.php~~
	1. ~~Change FileWatcherDB config.js scripts path to point to ScriptToReadResponseDB.bat~~
	1. ~~Provide a phpExePath.txt file in FileWatcher, set it to the path of the PHP file installed in step #1~~


# Golive Checklist
1. ~~Upload new vendor data~~
1. ~~Switch active xampp from 5.6.3 to 7.2.21~~
1. ~~Backup DB~~
1. ~~Apply DB migration~~
1. Restart both FileWatchers
1. Maintain data:
	1. MasterApprovalLimit
	1. ~~Master GL Account maintain nominative list ~~
	1. T_User_Privilege
	1. T_Master_Type_Of_Services
	1. ~~T_Master_PPH	maintain GLAccount_Code, SKB, NPWP, CoD, SIUJK~~
	1. ~~T_Master_Integration~~

# After Golive Checklist
1. Test print voucher
1. Trial 1 cycle